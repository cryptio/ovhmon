package auth

import "ovhmon-installer/ovh/http"

type CredentialState string

const (
	CredentialStateExpired           CredentialState = "expired"
	CredentialStatePendingValidation CredentialState = "pendingValidation"
	CredentialStateRefused           CredentialState = "refused"
	CredentialStateValidated         CredentialState = "validated"
)

// Represents OVH's auth.Credential class
type Credential struct {
	ConsumerKey   string          `json:"consumerKey"`
	State         CredentialState `json:"state"`
	ValidationUrl string          `json:"validationUrl"`
}

// OVH's auth.AccessRule
type AccessRule struct {
	Method http.Method `json:"method"`
	Path   string      `json:"path"`
}
