package ovh

type Endpoint string

// Endpoints
const (
	EndpointOVHEU        Endpoint = "https://eu.api.ovh.com/1.0"
	EndpointOVHCA        Endpoint = "https://ca.api.ovh.com/1.0"
	EndpointOVHUS        Endpoint = "https://api.us.ovhcloud.com/1.0"
	EndpointKimsufiEU    Endpoint = "https://eu.api.kimsufi.com/1.0"
	EndpointKimsufiCA    Endpoint = "https://ca.api.kimsufi.com/1.0"
	EndpointSoyoustartEU Endpoint = "https://eu.api.soyoustart.com/1.0"
	EndpointSoyoustartCA Endpoint = "https://ca.api.soyoustart.com/1.0"
	EndpointRunaboveCA   Endpoint = "https://api.runabove.com/1.0"
)

// Endpoints conveniently maps endpoints names to their URI for external configuration
var Endpoints = map[string]Endpoint{
	"ovh-eu":        EndpointOVHEU,
	"ovh-ca":        EndpointOVHCA,
	"ovh-us":        EndpointOVHUS,
	"kimsufi-eu":    EndpointKimsufiEU,
	"kimsufi-ca":    EndpointKimsufiCA,
	"soyoustart-eu": EndpointSoyoustartEU,
	"soyoustart-ca": EndpointSoyoustartCA,
	"runabove-ca":   EndpointRunaboveCA,
}
