package http

type Method string

const (
	HttpMethodDelete Method = "DELETE"
	HttpMethodGet    Method = "GET"
	HttpMethodPost   Method = "POST"
	HttpMethodPut    Method = "PUT"
)
