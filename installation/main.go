// Copyright 2019 cpke

package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-ini/ini"
	"io/ioutil"
	"net/http"
	"os"
	"ovhmon/installation/ovh"
	"ovhmon/installation/ovh/auth"
	"strings"
)

// createCredential attempts to request a token from OVH that authorizes us to use the API
func createCredential(endpoint ovh.Endpoint, accessRules []auth.AccessRule, applicationKey string) (auth.Credential, error) {
	url := fmt.Sprintf("%s/auth/credential", endpoint)
	// THe data to send in JSON format
	jsonValue, err := json.Marshal(struct {
		AccessRules []auth.AccessRule `json:"accessRules"`
	}{
		AccessRules: accessRules,
	})
	if err != nil {
		return auth.Credential{}, err
	}
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(jsonValue))
	if err != nil {
		return auth.Credential{}, err
	}
	req.Header.Set("Content-Type", "application/json")
	// Send the application key to request new credentials
	req.Header.Add("X-Ovh-Application", applicationKey)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return auth.Credential{}, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return auth.Credential{}, err
	}

	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		// We received a successful HTTP status code in the response, try to unmarshal the body into an auth.Credential
		// object
		var credential auth.Credential
		err = json.Unmarshal(body, &credential)
		if err != nil {
			return auth.Credential{}, err
		}
		return credential, nil
	} else {
		var message struct {
			Message string `json:"message"`
		}
		err = json.Unmarshal(body, &message)
		if err != nil {
			return auth.Credential{}, err
		}
		err = errors.New(fmt.Sprintf("Error %d: %s", resp.StatusCode, message.Message))
	}

	return auth.Credential{}, err
}

// Displays the possible API endpoints and prompts the user to select one.
// The endpoint name along with its corresponding value is returned
func selectEndpoint() (string, ovh.Endpoint) {
	reader := bufio.NewReader(os.Stdin)

	var endpointName string
	var endpointVal ovh.Endpoint
	endpointChosen := false

	// while the user has not chosen a valid endpoint
	for !endpointChosen {
		fmt.Println("[!] Endpoints:")
		for key, _ := range ovh.Endpoints {
			fmt.Printf("\t%s\n", key)
		}
		fmt.Println("[?] Which endpoint do you want to use? ")
		endpointName, _ = reader.ReadString('\n')
		endpointName = strings.TrimSpace(strings.ToLower(endpointName))

		endpointVal, endpointChosen = ovh.Endpoints[endpointName]
		if !endpointChosen {
			fmt.Println("[-] The provided endpoint is invalid")
		}
	}

	return endpointName, endpointVal
}

// This function asks the user to input the OVH application key and secret.
// It returns the inputted data.
func demandKeys() (string, string) {
	reader := bufio.NewReader(os.Stdin)

	fmt.Println("[?] Please enter the application key: ")
	applicationKey, _ := reader.ReadString('\n')
	// Remove any trailing newline characters
	applicationKey = strings.TrimSuffix(applicationKey, "\n")

	fmt.Println("[?] Please enter the application secret: ")
	applicationSecret, _ := reader.ReadString('\n')
	applicationSecret = strings.TrimSuffix(applicationSecret, "\n")

	return applicationKey, applicationSecret
}

// getDiscordConfiguration prompts the user to enter values for the INI fields we expect under the "discord" section
func getDiscordConfiguration() map[string]string {
	discordFriendlyKeyMap := map[string]string{
		"webhook URL":         "DISCORD_WEBHOOK",
		"profile picture URL": "DISCORD_ICON_URL",
		"username":            "DISCORD_USERNAME",
		"author name":         "DISCORD_AUTHOR_NAME",
		"message title":       "DISCORD_TITLE",
		"title URL":           "DISCORD_URL",
		"footer text":         "DISCORD_FOOTER_TEXT",
	}

	fmt.Println("[!] Prompting for Discord options")
	discordIniMap := getValuesMap(discordFriendlyKeyMap)

	return discordIniMap
}

// getValuesMap takes a map[string]string, with the key being a friendly name for the value, and prompts the user for each value.
// It returns a map where the key is the passed in map's value, and the value is the user's input
func getValuesMap(friendlyValuesMap map[string]string) map[string]string {
	iniMap := make(map[string]string)

	reader := bufio.NewReader(os.Stdin)

	for friendlyText, iniKey := range friendlyValuesMap {
		fmt.Printf("[?] Please enter a value for the %s: ", friendlyText)
		input, _ := reader.ReadString('\n')
		input = strings.TrimSuffix(input, "\n")
		iniMap[iniKey] = input
	}

	return iniMap
}

// getDiscordConfiguration prompts the user to enter values for the INI fields we expect under the "general" section
func getGeneralConfiguration() map[string]string {
	generalFriendlyKeyMap := map[string]string{
		"attack detection message": "ATTACK_MESSAGE",
		"attack recovery message":  "RECOVER_MESSAGE",
	}

	fmt.Println("[!] Prompting for general options")
	generalIniMap := getValuesMap(generalFriendlyKeyMap)

	return generalIniMap
}

// readYesNoResponse demands that the user enters either "y" or "n" for a response until they succeed
func readYesNoResponse(reader *bufio.Reader) bool {
	validResponse := false
	var response string
	for !validResponse {
		responseBuf, _ := reader.ReadString('\n')
		response = strings.TrimSpace(strings.ToLower(responseBuf))
		if response == "y" || response == "n" {
			validResponse = true
		} else {
			fmt.Println("Please respond with either y or n")
		}
	}

	return response == "y"
}

// The following function takes the credentials and endpoint for an OVH application and writes it to an INI configuration
// file that OVHMon can read
func writeIniSection(sectionName string, sectionData map[string]string, configName string) error {
	cfg, err := ini.Load(configName)
	if err != nil {
		return err
	}

	// The section will be created if it does not exist
	section, err := cfg.NewSection(sectionName)
	if err != nil {
		return err
	}

	for key, value := range sectionData {
		_, err := section.NewKey(key, value)
		if err != nil {
			return err
		}
	}

	// Save the config to the file
	err = cfg.SaveTo(configName)

	return err
}

// handleIniError handles the event where the program is unable to update the INI configuration file.
// It should be called when this happens so the process of setting up OVHMon can be finished
func handleIniError(sectionName string, sectionData map[string]string) {
	// If the application failed to write the data to the configuration file, we have to dump the required information
	// and instruct the user to manually complete it

	fmt.Println("[-] The application failed to modify the INI configuration file.")
	fmt.Println("Please add the following content to the file:")
	fmt.Printf("[%s]\n", sectionName)
	for key, value := range sectionData {
		fmt.Printf("%s = %s\n", key, value)
	}
}

func main() {
	finished := false
	reader := bufio.NewReader(os.Stdin)

	// The following access rules are required for the monitor
	accessRules := []auth.AccessRule{
		{
			Method: "GET",
			Path:   "/*",
		},
		{
			Method: "PUT",
			Path:   "/*",
		},
		{
			Method: "POST",
			Path:   "/*",
		},
	}

	const configName = "config.ini"
	// Create the file if it doesn't exist
	fd, _ := os.OpenFile(configName, os.O_RDONLY|os.O_CREATE, 0666)
	_ = fd.Close()

	discordOptions := getDiscordConfiguration()
	err := writeIniSection("discord", discordOptions, configName)
	if err != nil {
		fmt.Printf("Error: %s\n", err.Error())
		handleIniError("discord", discordOptions)
		return
	}
	fmt.Println("[+] Successfully saved Discord configurations")

	generalOptions := getGeneralConfiguration()

	fmt.Println("[?] Send attack throughput? (y/N) ")
	sendAttackThroughput := readYesNoResponse(reader)
	if sendAttackThroughput {
		generalOptions["SENDBPS"] = "true"
		err = writeIniSection("general", generalOptions, configName)
		if err != nil {
			fmt.Printf("Error: %s\n", err.Error())
			handleIniError("general", generalOptions)
			return
		}
	}

	fmt.Println("[+] Successfully saved general configurations")

	for !finished {
		endpointName, endpointVal := selectEndpoint()

		// The endpoint will be something like ca.api.ovh.com/1.0, but the createApp page is located at
		// ca.api.ovh.com/createApp, so we remove the "/1.0" part
		// 1 - the number of times to replace
		fmt.Printf("[!] Please go to %s/createApp to create an application for OVHMon.\nPress the enter key to continue once finished\n", strings.Replace(string(endpointVal), "/1.0", "", 1))
		// Do nothing with the result, we just want to wait for the enter key to be pressed
		_, _ = reader.ReadString('\n')

		applicationKey, applicationSecret := demandKeys()

		credential, err := createCredential(endpointVal, accessRules, applicationKey)
		if err != nil {
			fmt.Printf("[-] Error: %s\n", err.Error())
			return
		}

		fmt.Printf("[+] Credential request created, please complete it here:\n%s\n", credential.ValidationUrl)
		fmt.Printf("Please ensure that you set the validity to \"Unlimited\". Press the enter key when done\n")
		_, _ = reader.ReadString('\n')

		ovhIniMap := map[string]string{
			"OVH_APPLICATION_KEY":    applicationKey,
			"OVH_APPLICATION_SECRET": applicationSecret,
			"OVH_CONSUMER_KEY":       credential.ConsumerKey,
		}
		err = writeIniSection(endpointName, ovhIniMap, configName)
		if err != nil {
			fmt.Printf("[-] Error: %s\n", err.Error())
			handleIniError(endpointName, ovhIniMap)
			return
		}

		fmt.Println("[+] Successfully configured OVHMon")
		fmt.Println("[?] Do you want to add another endpoint to OVHMon? (y/N) ")
		finished = readYesNoResponse(reader)
	}
}
