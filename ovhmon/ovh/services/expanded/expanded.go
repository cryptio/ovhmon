// Copyright 2019 cpke

// Package expanded defines expanded types for OVH's services endpoint
package expanded

import (
	"math/big"
	"time"
)

// Represents OVH's services.expanded.Billing data type
type Billing struct {
	NextBillingDate time.Time `json:"nextBillingDate"`
	ExpirationDate  time.Time `json:"expirationDate"`
	Plan            Plan      `json:"plan"`
}

// Represents OVH's services.expanded.Plan data type
type Plan struct {
	Code        string `json:"code"`
	InvoiceName string `json:"invoiceName"`
}

// Represents OVH's services.expanded.Resource data type
type Resource struct {
	DisplayName string  `json:"displayName"`
	Name        string  `json:"name"`
	Product     Product `json:"product"`
}

// Represents OVH's services.expanded.Product data type
type Product struct {
	Description string `json:"description"`
	Name        string `json:"name"`
}

// Represents OVH's services.expanded.Route data type
type Route struct {
	Path string                   `json:"path"`
	Url  string                   `json:"url"`
	Vars []map[string]interface{} `json:"vars"`
}

// Represents OVH's services.expanded.Service data type
type Service struct {
	Billing   Billing  `json:"billing"`
	Resource  Resource `json:"resource"`
	Route     Route    `json:"route"`
	ServiceID big.Int  `json:"serviceId"`
}
