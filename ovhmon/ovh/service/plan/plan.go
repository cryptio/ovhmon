// Package plan defines structures for OVH plans
package plan

// Represents OVH's service.plan.Product data type
type Product struct {
	Name string `json:"name"`
}
