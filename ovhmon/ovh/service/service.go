// Package service defines data structures related to OVH services
package service

import (
	"math/big"
	"ovhmon/ovhmon/ovh/service/plan"
	"ovhmon/ovhmon/ovh/service/renew"
)

// Represents OVH's service.BillingState enum
type BillingState string

// Represents OVH's service.ResourceState enum
type ResourceState string

const (
	BillingStateExpired BillingState = "expired"
	BillingStateOk      BillingState = "ok"
	BillingStatePending BillingState = "pending"
	BillingStateUnpaid  BillingState = "unpaid"
)

const (
	ResourceStateDeleted    ResourceState = "deleted"
	ResourceStateDeleting   ResourceState = "deleting"
	ResourceStateOk         ResourceState = "ok"
	ResourceStateOpening    ResourceState = "opening"
	ResourceStateSuspended  ResourceState = "suspended"
	ResourceStateSuspending ResourceState = "suspending"
	ResourceStateToDelete   ResourceState = "toDelete"
	ResourceStateToOpen     ResourceState = "toOpen"
	ResourceStateToSuspend  ResourceState = "toSuspend"
)

// Represents OVH's service.Route data type
type Route struct {
	Path string                   `json:"path"`
	Url  string                   `json:"url"`
	Vars []map[string]interface{} `json:"vars"`
}

// Represents OVH's service.Plan data type
type Plan struct {
	Code    string
	Product plan.Product
}

// Represents OVH's service.Resource data type
type Resource struct {
	DisplayName string        `json:"displayName"`
	State       ResourceState `json:"state"`
	Name        string        `json:"name"`
}

// Represents OVH's service.Rewnw data type
type Renew struct {
	PossibleModes     []renew.Mode     `json:"possibleModes"`
	Mode              renew.Mode       `json:"mode"`
	DayOfMonth        big.Int          `json:"dayOfMonth"`
	Interval          renew.Interval   `json:"interval"`
	PossibleIntervals []renew.Interval `json:"possibleIntervals"`
}
