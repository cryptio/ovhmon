// Package renew provides renewal information for a service
package renew

// Represents OVH's service.renew.Mode enum
type Mode string

// Represents OVH's service.renew.Interval enum
type Interval string

const (
	ModeAutomaticForcedProduct = "automaticForcedProduct"
	ModeAutomaticV2012         = "automaticV2012"
	ModeAutomaticV2014         = "automaticV2014"
	ModeAutomaticV2016         = "automaticV2016"
	ModeDeleteAtEndEngagement  = "deleteAtEndEngagement"
	ModeDeleteAtExpiration     = "deleteAtExpiration"
	ModeManual                 = "manual"
	ModeOneShot                = "oneShot"
	ModeOption                 = "option"
)

const (
	IntervalPay1Month  = "P1M"
	IntervalPay3Months = "P3M"
	IntervalPay6Months = "P6M"
	IntervalPay1Year   = "P1Y"
	IntervalPay2Years  = "P2Y"
	IntervalPay3Years  = "P3Y"
)
