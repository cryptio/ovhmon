// Copyright 2019 cpke

// Package ip defines IP-related structures for OVH's ip endpoint
package ip

type MitigationProfileAutoMitigationTimeOut int
type MitigationStatsScale string

const (
	AutoMitigationTimeOutSeconds0  MitigationProfileAutoMitigationTimeOut = 0
	AutoMitigationTimeOutSeconds15 MitigationProfileAutoMitigationTimeOut = 15
	AutoMitigationTimeOutMinutes1  MitigationProfileAutoMitigationTimeOut = 60
	AutoMitigationTimeOutMinutes6  MitigationProfileAutoMitigationTimeOut = 360
	AutoMitigationTimeOutMinutes26 MitigationProfileAutoMitigationTimeOut = 1560
)

const (
	MitigationStatsScaleSeconds10 MitigationStatsScale = "10s"
	MitigationStatsScaleMinutes1  MitigationStatsScale = "1m"
	MitigationStatsScaleMinutes5  MitigationStatsScale = "5m"
)

// Represents OVH's ip.Ip data type
type Ip struct {
	CanBeTerminated bool     `json:"canBeTerminated"`
	OrganisationID  string   `json:"organisationID"`
	Country         string   `json:"country"`
	RoutedTo        RoutedTo `json:"routedTo"`
	Description     string   `json:"description"`
	Type            string   `json:"type"`
}

type RoutedTo struct {
	ServiceName string `json:"serviceName"`
}

// Represents OVH's ip.MitigationIp data type
type MitigationIp struct {
	Permanent      bool   `json:"permanent"`
	IpOnMitigation string `json:"ipOnMitigation"`
	State          string `json:"state"`
	Auto           bool   `json:"auto"`
}

// OVH's Ip.MitigationStats response class
type MitigationStats struct {
	Out       MitigationTraffic `json:"out"`
	Timestamp int64             `json:"timestamp"`
	In        MitigationTraffic `json:"in"`
}

// Represents OVH's ip.MitigationDetailedStats
type MitigationDetailedStats struct {
	Syn       bool              `json:"permanent"`
	In        MitigationTraffic `json:"in"`
	IcmpType  int64             `json:"icmpType"`
	Out       MitigationTraffic `json:"out"`
	DestPort  int64             `json:"destPort"`
	IcmpCode  int64             `json:"icmpCode"`
	Protocol  int64             `json:"protocol"`
	SrcPort   int64             `json:"srcPort"`
	Fragments bool              `json:"fragments"`
}

// MitigationTraffic is representative of ip.MitigationTraffic
type MitigationTraffic struct {
	Pps int64 `json:"pps"`
	Bps int64 `json:"bps"`
}

// Represents ip.ReverseIp
type ReverseIp struct {
	Reverse   string `json:"reverse"`
	IpReverse string `json:"ipReverse"`
}

// ip.MitigationProfile data structure
type MitigationProfile struct {
	AutoMitigationTimeout MitigationProfileAutoMitigationTimeOut `json:"autoMitigationTimeOut"`
	IpMitigationProfile   string                                 `json:"ipMitigationProfile"` // ipv4
}
