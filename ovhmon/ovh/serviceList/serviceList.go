// serviceList defines service models
package serviceList

import (
	"ovhmon/ovhmon/ovh/service"
)

// Represents OVH's serviceList.Service data type
type Service struct {
	Route           service.Route        `json:"route"`
	NextBillingDate string               `json:"nextBillingDate"`
	Details         []string             `json:"details"`
	Plan            service.Plan         `json:"plan"`
	State           service.BillingState `json:"state"`
	EngagementDate  string               `json:"engagementDate"`
	Resource        service.Resource     `json:"resource"`
	CreationDate    string               `json:"creationDate"`
	Renew           service.Renew        `json:"nextBillingDate"`
	Quantity        int64                `json:"quantity"`
	ExpirationDate  string               `json:"expirationDate"`
}
