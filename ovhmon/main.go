// Copyright 2019 cpke

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/go-ini/ini"
	"github.com/go-redis/redis"
	"github.com/golang/glog"
	"github.com/ovh/go-ovh/ovh"
	"math/big"
	"net/http"
	"net/url"
	"ovhmon/ovhmon/discord"
	ip2 "ovhmon/ovhmon/ovh/ip"
	"ovhmon/ovhmon/ovh/serviceList"
	"strconv"
	"sync"
	"time"
)

// Clients holds all of the clients we need to use between functions
type Clients struct {
	OvhClient   *ovh.Client
	RedisClient *redis.Client
}

// The configurable options
type Options struct {
	GeneralOptions map[string]string
	DiscordPayload discord.WebhookPayload
}

// loadOvhClients attempts to load the credentials for all of the different OVH endpoints
// from an INI config file. If a section is not specified it will be skipped.
// Each section should have the required key-value pairs, otherwise it will raise
// an error.
func loadOvhClients(config *ini.File) ([]ovh.Client, error) {
	var ovhAccounts []ovh.Client
	for key, _ := range ovh.Endpoints {
		section, err := config.GetSection(key)

		// The section doesn't exist
		if err != nil {
			continue
		}

		applicationKey := section.Key("OVH_APPLICATION_KEY").String()

		applicationSecret := section.Key("OVH_APPLICATION_SECRET").String()

		consumerKey := section.Key("OVH_CONSUMER_KEY").String()

		client, err := ovh.NewClient(
			key,
			applicationKey,
			applicationSecret,
			consumerKey,
		)
		if err != nil {
			return ovhAccounts, err
		}

		ovhAccounts = append(ovhAccounts, *client)

	}
	return ovhAccounts, nil
}

// parseGeneralOptions turns a map of all of the keys and values under the section named "general"
func parseGeneralOptions(config *ini.File) map[string]string {
	section := config.Section("general")

	return section.KeysHash()
}

// Attempt to sync the mitigation profiles across all OVH services on all OVH accounts
func syncMitigationProfiles(ovhClients []ovh.Client, autoMitigationTimeout ip2.MitigationProfileAutoMitigationTimeOut) error {
	// For each endpoint (i.e ovh-ca)
	for _, client := range ovhClients {
		// Get all of our IPs
		var ipBlock []string
		err := client.Get("/ip", &ipBlock)
		if err != nil {
			return err
		}

		// Retrieve all of the IP addresses under mitigation
		for _, ip := range ipBlock {
			var ipsUnderMitigation []string
			// If an IP is in permanent mitigation mode we will be able to access its mitigation status using this endpoint
			err = client.Get(fmt.Sprintf("/ip/%s/mitigation", url.QueryEscape(ip)), &ipsUnderMitigation)
			if err != nil {
				glog.Errorf("Error on ip/%s/mitigation: %s\n", ip, err.Error())
				continue
			}
			for _, ipOnMitigation := range ipsUnderMitigation {
				var ipOnMitigationObject ip2.MitigationIp
				err = client.Get(fmt.Sprintf("/ip/%s/mitigation/%s", url.QueryEscape(ip), ipOnMitigation), &ipOnMitigationObject)
				if err != nil {
					glog.Errorf("Error on ip/%s/mitigation/%s: %s\n", ip, ipOnMitigation, err.Error())
					continue
				}

				mitigationProfileRequest := ip2.MitigationProfile{
					AutoMitigationTimeout: autoMitigationTimeout,
					IpMitigationProfile:   ipOnMitigation,
				}

				mitigationProfilesEndpoint := fmt.Sprintf("/ip/%s/mitigationProfiles", url.QueryEscape(ipOnMitigation))

				// Check to see if a mitigation profile already exists
				var mitigationProfiles []string
				err = client.Get(mitigationProfilesEndpoint, &mitigationProfiles)
				// Update the current mitigation profile if it already exists
				if len(mitigationProfiles) > 0 {
					glog.Infof("A mitigation profile for %s already exists, updating it...\n", ipOnMitigation)
					for _, mitigationProfile := range mitigationProfiles {
						err = client.Put(fmt.Sprintf("%s/%s", mitigationProfilesEndpoint, mitigationProfile), mitigationProfileRequest, nil)
						if err != nil {
							glog.Errorf("Error on ip/%s/mitigationProfiles/%s: %s\n", ipOnMitigation, mitigationProfile, err.Error())
						}
					}
				} else {
					// Create a new mitigation profile
					glog.Infof("Creating a mitigation profile for %s...\n", ipOnMitigation)
					var returnedProfile ip2.MitigationProfile
					err := client.Post(fmt.Sprintf("/ip/%s/mitigationProfiles", url.QueryEscape(ipOnMitigation)), mitigationProfileRequest, &returnedProfile)
					if err != nil {
						glog.Errorf("Error on ip/%s/mitigationProfiles: %s\n", ipOnMitigation, err.Error())
						continue
					}
					glog.Infof("Created profile: %+v\n", returnedProfile)
				}
			}
		}
	}

	return nil
}

// parseDiscordOptions parses the expected keys under the "discord" INI section and returns a payload for the webhook
func parseDiscordOptions(config *ini.File) (discord.WebhookPayload, error) {
	discordSection := config.Section("discord")
	discordWebhook, err := url.Parse(discordSection.Key("DISCORD_WEBHOOK").String())
	if err != nil {
		return discord.WebhookPayload{}, err
	}

	discordIconUrl, err := url.Parse(discordSection.Key("DISCORD_ICON_URL").String())
	if err != nil {
		return discord.WebhookPayload{}, err
	}

	discordUsername := discordSection.Key("DISCORD_USERNAME").String()
	discordAuthorName := discordSection.Key("DISCORD_AUTHOR_NAME").String()
	discordTitle := discordSection.Key("DISCORD_TITLE").String()

	discordTitleUrl, err := url.Parse(discordSection.Key("DISCORD_URL").String())
	if err != nil {
		return discord.WebhookPayload{}, err
	}

	discordFooterText := discordSection.Key("DISCORD_FOOTER_TEXT").String()

	return discord.WebhookPayload{
		WebhookUrl: *discordWebhook,
		IconUrl:    *discordIconUrl,
		Username:   discordUsername,
		AuthorName: discordAuthorName,
		Title:      discordTitle,
		TitleUrl:   *discordTitleUrl,
		FooterText: discordFooterText,
	}, nil
}

// byteCountBinary converts bits into a human-readable format
func byteCountBinary(b int64) string {
	const unit = 1024
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.1f %cb", float64(b)/float64(div), "KMGTPE"[exp])
}

// monitorServer monitors an OVH server that is in permanent mitigation mode. If the mitigation status stored
// in the redis database has changed since the last time it was checked, then it will fire off an alert
func monitorServer(wg *sync.WaitGroup, clients *Clients, options *Options, ip string) {
	defer wg.Done()

	var ipsUnderMitigation []string
	// If an IP is in permanent mitigation mode we will be able to access its mitigation status using this endpoint
	err := clients.OvhClient.Get(fmt.Sprintf("/ip/%s/mitigation", url.QueryEscape(ip)), &ipsUnderMitigation)
	if err != nil {
		glog.Errorf("Error on ip/%s/mitigation: %s\n", ip, err.Error())
		// This IP is unavailable, just return
		return
	}
	for _, ipOnMitigation := range ipsUnderMitigation {
		var ipOnMitigationObject ip2.MitigationIp
		err = clients.OvhClient.Get(fmt.Sprintf("/ip/%s/mitigation/%s", url.QueryEscape(ip), ipOnMitigation), &ipOnMitigationObject)
		if err != nil {
			glog.Errorf("Error on ip/%s/mitigation/%s: %s\n", ip, ipOnMitigation, err.Error())
		}
		// glog.Infof("%+v\n", ipOnMitigationObject)
		// When a DDoS attack is happening and mitigation is forced,
		// 	both "permanent" and "auto" are set to true, despite the fact that
		// 	only one of these values can be toggled from the control panel.
		forcedMitigation := ipOnMitigationObject.Permanent && ipOnMitigationObject.Auto
		redisKey := fmt.Sprintf("MITIGATION_STATUS_%s", ipOnMitigationObject.IpOnMitigation)
		val, err := clients.RedisClient.Get(redisKey).Result()
		if err != nil && err != redis.Nil {
			glog.Fatalf("Could not fetch the current mitigation value for %s: %s\n", ipOnMitigationObject.IpOnMitigation, err.Error())
		}

		var valBool bool
		if val == "" {
			valBool = false
		} else {
			// We could just to valBool = val == "true", but ParseBool supports more representations like TRUE, 1, etc.
			valBool, err = strconv.ParseBool(val)
			if err != nil {
				glog.Fatalf("Error parsing the value for key \"%s\": %s\n", redisKey, err.Error())
			}
		}

		if valBool != forcedMitigation { // The value has changed
			// Find the server in the database that has the same IP address
			// Get the friendly name
			// Send an alert

			// Get the details about the IP address
			var ipAddress ip2.Ip
			err = clients.OvhClient.Get(fmt.Sprintf("/ip/%s", ipOnMitigation), &ipAddress)
			if err != nil {
				glog.Errorf("Error fetching result from /ip/%s: %s\n", ipOnMitigation, err.Error())
			}
			// i.e ns613894.ip-163-69-24.net
			currentServiceName := ipAddress.RoutedTo.ServiceName

			// Get all of the services
			// The /service endpoint is not to be confused with /services, which returns inconsistent responses
			// 	between the OVH CA and OVH US API
			var services []big.Int
			err = clients.OvhClient.Get("/service", &services)
			if err != nil {
				glog.Errorf("Unable to fetch result from /service: %s\n", err.Error())
			}

			// Iterate over the listed services.
			// For each service, check if its name matches the service name we are looking for
			var serverName string
			for _, service := range services {
				var serviceDetails serviceList.Service
				err = clients.OvhClient.Get(fmt.Sprintf("/service/%s", service.String()), &serviceDetails)
				if err != nil {
					glog.Errorf("Error getting the service details: %s\n", err.Error())
				}
				if serviceDetails.Resource.Name == currentServiceName {
					// We have found the service
					serverName = serviceDetails.Resource.DisplayName
					break
				}
			}

			if serverName == "" {
				glog.Errorf("Error: Could not find corresponding display name for service %s", currentServiceName)
				serverName = ipOnMitigation
			}

			sendBps, err := strconv.ParseBool(options.GeneralOptions["SEND_BPS"])
			if err != nil {
				glog.Fatalf("Error parsing SEND_BPS value: %s\n", err.Error())
			}

			var bpsIn int64
			var bpsStr string
			// SEND_BPS is set to true and the IP address is under an attack
			if sendBps && forcedMitigation {
				now := time.Now()
				from := now.UTC().Format(time.RFC3339)
				// If we do 1 minute we will get the results for 1 minutes despite using a scale of 1 minute, so we must do 59 seconds
				// 	or less
				to := now.Add(time.Second * 59).UTC().Format(time.RFC3339)

				var mitigationStats []ip2.MitigationStats
				mitigationStatsEndpoint := fmt.Sprintf("/ip/%s/mitigation/%s/stats?from=%s&scale=%s&to=%s", url.QueryEscape(ip), ipOnMitigation, url.QueryEscape(from), ip2.MitigationStatsScaleMinutes1, url.QueryEscape(to))

				nMitigationStatsAttempts := 0
				const maxMitigationStatdAttempts = 3
				mitigationStatsSuccess := false

				// GoLang's equivalent of a while loop
				// Since OVH's backend is not the most reliable and will return an Internal Server Error, we
				// 	give it 3 chances to successfully retrieve the value
				for nMitigationStatsAttempts < maxMitigationStatdAttempts && !mitigationStatsSuccess {
					err = clients.OvhClient.Get(mitigationStatsEndpoint, &mitigationStats)

					if err != nil {
						glog.Errorf("Unable to get mitigation stats from %s: %s\n", mitigationStatsEndpoint, err.Error())
					} else {
						mitigationStatsSuccess = true
					}

					nMitigationStatsAttempts++
				}

				if len(mitigationStats) > 0 {
					bpsIn = mitigationStats[0].In.Bps

					// Given the number of bits per second as a long, byteCountBinary will transform it into a human-readable
					// 	bit-size (i.e 10 Mb). From there, we will add "ps" for "per second" to indicate that this is the throughput
					bpsStr = fmt.Sprintf("%sps", byteCountBinary(bpsIn))
				} else {
					glog.Errorln("mitigationStats len is 0")
				}
			}

			var discordDescription string
			var alertColor int
			if forcedMitigation {
				discordDescription = options.GeneralOptions["ATTACK_MESSAGE"]

				alertColor = 16711680
			} else {
				discordDescription = options.GeneralOptions["RECOVER_MESSAGE"]
				alertColor = 3329330
			}

			discordData := map[string]interface{}{
				"username":   options.DiscordPayload.Username,
				"avatar_url": options.DiscordPayload.IconUrl.String(),
				"file":       "content",
				"embeds": []map[string]interface{}{
					{
						"author": map[string]interface{}{
							"name":     options.DiscordPayload.AuthorName,
							"url":      "",
							"icon_url": options.DiscordPayload.IconUrl.String(),
						},
						"fields": []map[string]interface{}{
							{
								"name":   "Server",
								"value":  serverName,
								"inline": true,
							},
							{
								"name":   "Provider",
								"value":  "OVH",
								"inline": true,
							},
						},
						"color":       alertColor,
						"title":       options.DiscordPayload.Title,
						"url":         options.DiscordPayload.TitleUrl.String(),
						"description": discordDescription,
						"timestamp":   time.Now().UTC(),
						"footer": map[string]interface{}{
							"text":     options.DiscordPayload.FooterText,
							"icon_url": options.DiscordPayload.IconUrl.String(),
						},
					},
				},
			}

			if forcedMitigation && bpsIn > 0 {
				embeds := discordData["embeds"].([]map[string]interface{})
				fields := embeds[0]["fields"].([]map[string]interface{})
				fields = append(fields, map[string]interface{}{
					"name":   "Throughput",
					"value":  bpsStr,
					"inline": false,
				})
				discordData["embeds"].([]map[string]interface{})[0]["fields"] = fields
			}

			jsonData, err := json.Marshal(discordData)
			if err != nil {
				glog.Fatalf("Error marshalling discordData into JSON: %s\n", err.Error())
			}

			_, err = http.Post(options.DiscordPayload.WebhookUrl.String(), "application/json", bytes.NewBuffer(jsonData))
			if err != nil {
				glog.Fatalf("Unable to send alert: %s\n", err.Error())
			}
			glog.Infof("Successfully sent alert for %s\n", ipAddress)
		}
		// Update the stored mitigation value of the IP address in redis
		clients.RedisClient.Set(redisKey, forcedMitigation, 0)
	}
}

// monitorClient monitors all IP addresses under an OVH client
func monitorClient(wg *sync.WaitGroup, clients *Clients, options *Options) {
	defer wg.Done()

	for {
		// Get all of our IPs
		var ipBlock []string
		err := clients.OvhClient.Get("/ip", &ipBlock)
		if err != nil {
			glog.Errorf("Error on /ip: %s\n", err.Error())
		}

		var ipWg sync.WaitGroup

		// Retrieve all of the IP addresses under mitigation
		for _, ip := range ipBlock {
			ipWg.Add(1)
			go monitorServer(&ipWg, clients, options, ip)
		}

		ipWg.Wait()

		time.Sleep(time.Second * 5) // Wait 5 seconds
	}
}

func main() {
	cfg, err := ini.Load("config.ini")
	if err != nil {
		glog.Fatalf("Error: loading the configuration file %s\n", err.Error())
	}

	ovhClients, err := loadOvhClients(cfg)
	if err != nil {
		glog.Fatalf("Error loading the OVH clients: %s\n", err.Error())
	}

	discordWebhook, err := parseDiscordOptions(cfg)
	if err != nil {
		glog.Fatalf("Unable to parse Discord options: %s\n", err.Error())
	}

	generalOptions := parseGeneralOptions(cfg)

	redisClient := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	// err = syncMitigationProfiles(ovhClients, ip2.AutoMitigationTimeOutSeconds0)
	// if err != nil {
	// 	glog.Errorf("Error syncing mitigation profiles: %s", err.Error())
	// }

	options := Options{
		GeneralOptions: generalOptions,
		DiscordPayload: discordWebhook,
	}

	var wg sync.WaitGroup

	// For each endpoint (i.e ovh-ca)
	for _, client := range ovhClients {
		// We have to copy the iterator variable, client, over to a new variable. This is because clients is
		// constructed using a reference, and if we pass in a reference to a loop iterator variable then we end up
		// passing in the same address every time:
		// https://github.com/golang/go/wiki/CommonMistakes
		currentClient := client

		clients := Clients{OvhClient: &currentClient, RedisClient: redisClient}

		// Start a new thread for each client
		wg.Add(1)
		go monitorClient(&wg, &clients, &options)
	}

	// Wait for all Goroutines to finish
	wg.Wait()
}
