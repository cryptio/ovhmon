// Copyright 2019 cpke

// Package discord defines data structures to assist in the delivery of webhook data
package discord

import "net/url"

// WebhookPayload is a simplified structure that holds
// limited data for values that can be sent to a Discord webhook
type WebhookPayload struct {
	WebhookUrl url.URL
	IconUrl url.URL
	Username string
	AuthorName string
	Title string
	TitleUrl url.URL
	FooterText string
}
